﻿using System.Threading.Tasks;
using System.Web.Http;
using ForeNet.Business.Commands.CreateOrder;
using MediatR;

namespace ForeNet.Web.Api.Controllers
{
    public class PaymentsController : ApiController
    {
        private readonly IMediator _mediator;

        public PaymentsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        //[SwaggerResponse(200)]
        [Route("api/Payments/CreateOrder")]
        public async Task<IHttpActionResult> CreateOrder(CreateOrderCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

    }
}
