﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ForeNet.Business.Commands.CreateOrder;
using MediatR;
using MediatR.Pipeline;
using SimpleInjector;

namespace ForeNet.Web.Api
{
    public static class IOC
    {
       
        private static Container _container;

        public static Container Container { get; } = BuildContainer();

        private static Container BuildContainer()
        {
            var container = new Container();
            BuildMediator(container);
            return container;
        }

        private static void BuildMediator(Container container)
        {
            var assemblies = GetAssemblies().ToList();
         
            container.RegisterSingleton<IMediator, Mediator> ();
            container.Register (typeof (IRequestHandler<,>), assemblies);

            // we have to do this because by default, generic type definitions (such as the Constrained Notification Handler) won't be registered
            var notificationHandlerTypes = container.GetTypesToRegister (typeof (INotificationHandler<>), assemblies, new TypesToRegisterOptions {
                IncludeGenericTypeDefinitions = true,
                IncludeComposites = false,
            });
            container.Register (typeof (INotificationHandler<>), notificationHandlerTypes);

            container.Collection.Register (typeof (IPipelineBehavior<,>), Enumerable.Empty<Type> ());
            container.Collection.Register (typeof (IRequestPreProcessor<>), Enumerable.Empty<Type> ());
            container.Collection.Register (typeof (IRequestPostProcessor<,>), Enumerable.Empty<Type> ());

            container.Register (() => new ServiceFactory (container.GetInstance), Lifestyle.Singleton);
            
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(IMediator).GetTypeInfo().Assembly;
            yield return typeof(CreateOrderCommand).GetTypeInfo().Assembly;
        }
    }
}
