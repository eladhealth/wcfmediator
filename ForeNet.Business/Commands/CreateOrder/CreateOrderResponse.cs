﻿namespace ForeNet.Business.Commands.CreateOrder
{
    public class CreateOrderResponse
    {
        public int OrderId { get; set; }
    }
}