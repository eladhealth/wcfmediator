﻿namespace ForeNet.Business.Commands.CreateOrder
{
    public class CreateOrderCommand: ICommand<CreateOrderResponse>
    {
        public string Name { get; set; }
    }
}
