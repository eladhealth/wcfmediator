﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace ForeNet.Business.Commands.CreateOrder
{
    public class CreateOrderCommandHandler: IRequestHandler<CreateOrderCommand, CreateOrderResponse>
    {
        public async Task<CreateOrderResponse> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            return new CreateOrderResponse
            {
                OrderId = new Random(DateTime.Now.Millisecond).Next()
            };
        }
    }
}
