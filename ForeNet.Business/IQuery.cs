﻿using MediatR;

namespace ForeNet.Business
{
    public interface IQuery<T>: IRequest<T>
    {
    }
}