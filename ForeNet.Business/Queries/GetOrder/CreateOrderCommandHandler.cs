﻿using System.Threading;
using System.Threading.Tasks;
using ForeNet.Business.Models;
using MediatR;

namespace ForeNet.Business.Queries.GetOrder
{
    public class GetOrderByIdQueryHandler: IRequestHandler<GetOrderByIdQuery, GetOrderByIdResponse>
    {
        public async Task<GetOrderByIdResponse> Handle(GetOrderByIdQuery request, CancellationToken cancellationToken)
        {
            return new GetOrderByIdResponse
            {
                Order = new OrderDto()
            };
        }
    }
}
