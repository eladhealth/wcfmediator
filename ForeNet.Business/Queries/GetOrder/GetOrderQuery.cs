﻿namespace ForeNet.Business.Queries.GetOrder
{
    public class GetOrderByIdQuery: IQuery<GetOrderByIdResponse>
    {
        public int OrderId { get;set; }
    }
}
