﻿using ForeNet.Business.Models;

namespace ForeNet.Business.Queries.GetOrder
{
    public class GetOrderByIdResponse
    {
        public OrderDto Order { get; set; }
    }
}