﻿using MediatR;

namespace ForeNet.Business
{
    public interface ICommand<out T>: IRequest<T>
    {
    }
}