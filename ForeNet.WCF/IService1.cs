﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.Threading.Tasks;

namespace ForeNet.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        Task<string> Execute(string requestType, string json);
    }
}
