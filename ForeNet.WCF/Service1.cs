﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ForeNet.Business.Commands.CreateOrder;
using ForeNet.WCF.Infrastructure;
using MediatR;
using Newtonsoft.Json;

namespace ForeNet.WCF
{
    public class Service1 : IService1
    {
        private static readonly Dictionary<string, Type> RequestNamesToTypes;

        private static readonly IMediator Mediator;
        static Service1()
        {
            Mediator = IOC.Container.GetInstance<IMediator>();

            var allRequestTypes = from t in typeof(CreateOrderCommand).Assembly.GetTypes()
                                  where t.IsAbstract == false && t.IsInterface == false && typeof(IBaseRequest).IsAssignableFrom(t)
                                  select t;

            //we map the request type bv it's name. if you think it won't be unique, add an attribute to the command and
            //read from it.
            var dictionary = allRequestTypes.ToDictionary(type => type.Name.ToUpperInvariant(), type => type);
            RequestNamesToTypes = dictionary;
        }

        public async Task<string> Execute(string requestType, string json)
        {
            var foundMatchingRequest = RequestNamesToTypes.TryGetValue((requestType ?? "").ToUpperInvariant(), out Type type);
            if (foundMatchingRequest == false)
            {
                throw new Exception("Could not find a request Type matching " + requestType);
            }

            dynamic request = JsonConvert.DeserializeObject(json ?? "{}", type);
            
            var response = await Mediator.Send(request);

            var responseText = JsonConvert.SerializeObject(response);

            return responseText;
        }
    }
}
